#!/bin/bash

if [ $# -ne 1 ]
then
	echo "$0 <Product>"
	exit 1
fi
for id in background background-hd background-iPad background-iPad-hd
do 
	/Applications/Inkscape.app/Contents/Resources/bin/inkscape -i ${id} -t /Users/bcdlog/Documents/workspace-Sites/Documents-Images/Images/Puzzle/${1} -e /Users/bcdlog/Documents/Puzzle/puzzle/products/${1}/${id}.png | grep "was not found in the document"
done

for id in pieces pieces-hd pieces-iPad pieces-iPad-hd
do 
	/Applications/Inkscape.app/Contents/Resources/bin/inkscape -i ${id} -t /Users/bcdlog/Documents/workspace-Sites/Documents-Images/Images/Puzzle/${1} -e /Users/bcdlog/Documents/Puzzle/puzzle/products/${1}/${id}.png | grep "was not found in the document"
done

for n in 12 30 64
do
	for id in pieces-${n} pieces-${n}-hd pieces-${n}-iPad pieces-${n}-iPad-hd
	do 
		/Applications/Inkscape.app/Contents/Resources/bin/inkscape -i ${id} -t /Users/bcdlog/Documents/workspace-Sites/Documents-Images/Images/Puzzle/${1} -e /Users/bcdlog/Documents/Puzzle/puzzle/products/${1}/${id}.png | grep "was not found in the document"
	done
done
